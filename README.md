# Netgsm

An extended SMS plugin for sending Netgsm SMS.

# Installation
 1. Download [SMS Framework][sms-framework] and its dependencies.
 2. Install module as per [standard procedure][drupal-module-install].
 3. This module has composer dependencies,
    see [instructions][composer-dependencies]
    for information on how to resolve these dependencies.

[sms-framework]: https://drupal.org/project/smsframework
[drupal-module-install]: https://www.drupal.org/docs/8/extending-drupal/installing-contributed-modules
"Installing Contributed Modules"
[composer-dependencies]: https://www.drupal.org/docs/8/extending-drupal/installing-modules-composer-dependencies
"Installing modules' Composer dependencies"

<?php

namespace Drupal\sms_netgsm\Plugin\SmsGateway;

use Drupal\Component\Utility\Html;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\sms\Message\SmsDeliveryReport;
use Drupal\sms\Message\SmsMessageInterface;
use Drupal\sms\Message\SmsMessageResult;
use Drupal\sms\Plugin\SmsGatewayPluginBase;
use Drupal\sms_netgsm\NetgsmSms;
use Drupal\sms_netgsm\NetgsmSmsReport;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements Netgsm gateway.
 *
 * @SmsGateway(
 *   id = "netgsm",
 *   label = @Translation("Netgsm"),
 * )
 */
class Netgsm extends SmsGatewayPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Define constants for this class.
   */
  const SUCCESS = 200;
  const BAD_REQUEST = 400;
  const UNAUTHORIZED = 401;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * Constructs a new Netgsm plugin.
   *
   * @param array $configuration
   *   The configuration to use and build the sms gateway.
   * @param string $plugin_id
   *   The gateway id.
   * @param mixed $plugin_definition
   *   The gateway plugin definition.
   * @param \GuzzleHttp\ClientInterface $client
   *   The HTTP client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ClientInterface $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'username' => '',
      'password' => '',
      'sender' => 'Netgsm',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['netgsm'] = [
      '#type' => 'details',
      '#title' => $this->t('Netgsm'),
      '#open' => TRUE,
    ];
    $form['netgsm']['username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Username'),
      '#default_value' => $config['username'],
      '#required' => TRUE,
    ];
    $form['netgsm']['password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Password'),
      '#default_value' => $config['password'],
      '#required' => TRUE,
    ];
    $form['netgsm']['sender'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sender'),
      '#default_value' => $config['sender'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // Sanitize the credentials.
    $username = Html::escape($form_state->getValue('username'));
    $password = Html::escape($form_state->getValue('password'));
    $sender = Html::escape($form_state->getValue('sender'));

    // Save the credentials.
    $this->configuration['username'] = trim($username);
    $this->configuration['password'] = trim($password);
    $this->configuration['sender'] = trim($sender);
  }

  /**
   * {@inheritdoc}
   */
  public function send(SmsMessageInterface $sms_message) {
    $result = new SmsMessageResult();
    $report = new SmsDeliveryReport();
    $sender = $this->configuration['sender'];

    // Get the message plain text.
    $message = $sms_message->getMessage();

    // Get the Unix timestamp.
    $timestamp = \time();

    $sms_gateway = \Drupal::entityTypeManager()
      ->getStorage('sms_gateway')
      ->load($this->getPluginId());

    $sms_message->setSender($sender);
    $sms_message->setSenderNumber($sender);
    $sms_message->setGateway($sms_gateway);

    foreach ($sms_message->getRecipients() as $recipient) {
      $sms = new NetgsmSms();
      $response = $sms
        ->setClient($this->client)
        ->setCredentials([
          'usercode' => $this->configuration['username'],
          'password' => $this->configuration['password'],
        ])
        ->setSender($sender)
        ->setRecipient($recipient)
        ->setMessage($message)
        ->send();

      if ($response) {
        $report->setStatusMessage($message);
        $report->setStatusTime($timestamp);
        $report->setRecipient($recipient);
      }

      return $response;
    }

    return $result;
  }

  /**
   * Get SMS report by bulk ID.
   *
   * @param string $bulk_id
   *   The bulk ID for which the SMS report is retrieved.
   *
   * @return array
   *   Returns an array containing the SMS report based on the bulk ID.
   */
  public function getReportByBulkId(string $bulk_id) {
    $report = new NetgsmSmsReport();
    $response = $report
      ->setClient($this->client)
      ->setCredentials([
        'usercode' => $this->configuration['username'],
        'password' => $this->configuration['password'],
      ])
      ->setBulkId($bulk_id)
      ->setDetail(1)
      ->setType(0)
      ->setVersion(2)
      ->getResponse();
    return $response;
  }

}

<?php

namespace Drupal\sms_netgsm;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\sms\Plugin\SmsGatewayPluginManagerInterface;
use Symfony\Component\Routing\Route;

/**
 * Defines a custom parameter converter for loading third-party plugins.
 *
 * This converter retrieves SMS plugin settings based on the provided plugin ID
 * and validates the plugin definition using the SMS gateway manager.
 */
class ThirdPartyPluginConverter implements ParamConverterInterface {

  /**
   * The SMS gateway manager.
   *
   * @var \Drupal\sms\Plugin\SmsGatewayPluginManagerInterface
   */
  protected $pluginManager;

  /**
   * Constructs a ThirdPartyPluginConverter object.
   *
   * @param \Drupal\sms\Plugin\SmsGatewayPluginManagerInterface $plugin_manager
   *   The SMS gateway plugin manager service.
   */
  public function __construct(SmsGatewayPluginManagerInterface $plugin_manager) {
    $this->pluginManager = $plugin_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function convert($plugin_id, $definition, $name, array $defaults) {
    $enabled = FALSE;
    $settings = [];

    // Validate plugin definition and retrieve SMS plugin settings.
    if ($this->pluginManager->hasDefinition($plugin_id)) {
      $config = \Drupal::config('sms.gateway.' . $plugin_id);
      $enabled = $config->get('status');
      $settings = $config->get('settings') ?? [];
    }

    // Create plugin instance by plugin ID and settings.
    if ($enabled && $settings) {
      return $this->pluginManager->createInstance($plugin_id, $settings);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route) {
    return TRUE;
  }

}

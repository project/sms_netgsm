<?php

namespace Drupal\sms_netgsm;

use Drupal\Core\Url;

/**
 * Represents a report for Netgsm SMS.
 */
final class NetgsmSmsReport {

  use NetgsmClient;

  /**
   * The report bulk ID value.
   *
   * @var int
   */
  protected $bulkId;

  /**
   * The report detail value.
   *
   * @var int
   */
  protected $detail;

  /**
   * The report type value.
   *
   * @var int
   */
  protected $type;

  /**
   * The report version value.
   *
   * @var int
   */
  protected $version;

  /**
   * The report view value.
   *
   * @var int
   */
  protected $view;

  /**
   * Set the report bulk ID value.
   *
   * @param int $bulk_id
   *   The bulk ID value to be set.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setBulkId(int $bulk_id) {
    $this->bulkId = $bulk_id;
    return $this;
  }

  /**
   * Set the report detail value.
   *
   * When the value "1" is sent to the report procedure,
   * detailed report information of the sent SMS is returned.
   * It must be sent when making a request.
   *
   * @param int $detail
   *   The detail value to be set.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setDetail(int $detail) {
    $this->detail = $detail;
    return $this;
  }

  /**
   * Set the report type value.
   *
   * @param int $type
   *   The type value to be set.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setType(int $type) {
    $this->type = $type;
    return $this;
  }

  /**
   * Set the report version value.
   *
   * @param int $version
   *   The version value to be set.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setVersion(int $version) {
    $this->version = $version;
    return $this;
  }

  /**
   * Set the report view value.
   *
   * @param int $view
   *   The view value to be set.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setView(int $view) {
    $this->view = $view;
    return $this;
  }

  /**
   * Get the response from the URL and parse it based on a delta value.
   *
   * @param mixed $delta
   *   The delta value to determine specific tasks or 'all'
   *   tasks in the response.
   *
   * @see https://www.netgsm.com.tr/dokuman/#http-post-rapor
   *
   * @return array
   *   Returns an array containing parsed task information
   *   based on the delta value.
   */
  public function getResponse($delta = 0) {
    // Add status conditions.
    $conditions = $this->getStatusConditions();
    $this->setConditions($conditions);

    $response = $this->call('GET', Url::fromUri('https://api.netgsm.com.tr/sms/report'), [
      'bulkid' => $this->bulkId,
      'version' => $this->version,
      'type' => $this->type,
      'detail' => $this->detail,
    ]) ?? '';

    return $response ? $this->parseResponse($response, $delta) : NULL;
  }

  /**
   * Parse the response string into an array of tasks based on a delta value.
   *
   * @param string $response
   *   The response string to be parsed.
   * @param mixed $delta
   *   The delta value to determine specific tasks or 'all' tasks.
   *
   * @return array
   *   Returns an array containing parsed task information
   *   based on the delta value.
   */
  protected function parseResponse(string $response, $delta = 0) : array {
    $tasks = explode('<br>', $response);
    $result = [];
    if (!$tasks) {
      return $result;
    }
    if ($delta == 'all') {
      foreach ($tasks as $task) {
        if (!$task) {
          continue;
        }
        $result[] = $this->parseTaskList($task);
      }
    }
    elseif (isset($tasks[$delta])) {
      $result = $this->parseTaskList($tasks[$delta]);
    }
    return $result;
  }

  /**
   * Parse the task list string into an array.
   *
   * @param string $task
   *   The task list string to be parsed.
   *
   * @return array
   *   Returns an array containing parsed task information.
   */
  protected function parseTaskList(string $task) : array {
    $conditions = $this->conditions;
    $tasks = explode(' ', $task);
    $status_code = $tasks[6] ?? NULL;
    return [
      'arguments' => [
        'bulk_id' => $this->bulkId,
        'detail' => $this->detail,
        'type' => $this->type,
        'version' => $this->version,
        'view' => $this->view,
      ],
      'phone_number' => $tasks[0] ?? NULL,
      'message_size' => $tasks[3] ?? NULL,
      'message_status' => [
        'code' => $tasks[1] ?? NULL,
        'message' => isset($tasks[1]) && isset($conditions[$tasks[1]]) ? $conditions[$tasks[1]] : NULL,
      ],
      'operator_code' => $tasks[2] ?? NULL,
      'transmission_date' => $tasks[4] ?? NULL,
      'transmission_time' => $tasks[5] ?? NULL,
      'status' => [
        'code' => $status_code,
        'message' => $conditions[$status_code] ?? NULL,
      ],
      'error' => [
        'code' => $status_code,
        'message' => $conditions[$status_code] ?? NULL,
      ],
    ];
  }

  /**
   * Get status conditions for SMS report.
   *
   * @return array
   *   Returns an associative array containing SMS status conditions.
   */
  protected function getStatusConditions() : array {
    return [
      '0' => 'SMS delivery waiting to be sent.',
      '1' => 'SMS has been sent.',
      '2' => 'SMS delivery expired.',
      '3' => 'Incorrect or invalid phone number.',
      '4' => 'Could not be sent to operator.',
      '11' => 'SMS rejected by the operator.',
      '12' => 'SMS delivery failed.',
      '13' => 'Duplicate SMS.',
      '100' => 'All message statuses.',
      '103' => 'SMS delivery and other tasks has been failed.',
    ];
  }

}

<?php

namespace Drupal\sms_netgsm;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Provides functionality for interacting with the Netgsm service.
 */
trait NetgsmClient {

  use StringTranslationTrait;

  /**
   * The guzzle client.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * The conditions or status.
   *
   * @var array
   */
  protected $conditions = [];

  /**
   * The usercode and password.
   *
   * @var array
   */
  protected $credentials = [];

  /**
   * Set guzzle client interface.
   *
   * @param \GuzzleHttp\ClientInterface $client
   *   The guzzle client interface to make HTTP requests.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setClient(ClientInterface $client) : self {
    $this->client = $client;
    return $this;
  }

  /**
   * Set the conditions or status.
   *
   * @param array $conditions
   *   The API conditions or status codes.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setConditions(array $conditions) {
    $this->conditions = $conditions;
    return $this;
  }

  /**
   * Set the usercode and password.
   *
   * @param array $credentials
   *   The usercode and password for HTTP request authentication.
   *
   * @example
   *   [
   *     'usercode' => 'usercode',
   *     'password' => 'password',
   *   ]
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setCredentials(array $credentials) : self {
    $this->credentials = $credentials;
    return $this;
  }

  /**
   * Make an HTTP request.
   *
   * @param string $method
   *   The HTTP method to be used for the request (e.g., GET, POST).
   * @param \Drupal\Core\Url $url
   *   The URL object representing the request endpoint URL.
   * @param mixed|null $params
   *   (Optional) The parameters to be sent with the request, if applicable.
   * @param array $headers
   *   (Optional) An associative array of HTTP headers.
   *
   * @return string
   *   Returns the response content.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Thrown if error conditions found or HTTP request is not successful.
   */
  public function call(string $method, Url $url, $params = NULL, $headers = []) {
    $error_message = $this->t('SMS client encountered an error. Please try again later or contact the site administrator.');
    $content = '';

    // Set a default query options.
    $options = [
      'query' => [
        'usercode' => $this->credentials['usercode'],
        'password' => $this->credentials['password'],
      ],
    ];

    // Merge parameters into the 'query' options if $params is an array.
    if ($params && is_array($params)) {
      $options['query'] = array_merge($options['query'], $params);
    }

    // Set options for the URL object.
    $url->setOptions($options);

    // Attempt HTTP request with Guzzle, handle responses and exceptions.
    try {
      $response = $this->client->request($method, $url->toString(), [
        'headers' => $headers,
      ]);
      $content = $response->getBody()->getContents();

      // Get the Netgsm status and error conditions.
      $this->conditions += $this->getDefaultStatusConditions();
      $condition_content = $this->conditions[$content] ?? NULL;
      $exception_code = $content == '30' ? 400 : 401;

      if ($response->getStatusCode() !== 200) {
        throw new HttpException(400, $error_message);
      }

      // Throws an HTTP exception if error conditions
      // match the returned content.
      if ($condition_content) {
        throw new HttpException($exception_code, $condition_content);
      }

      return $content;
    }
    catch (HttpException $e) {
      \Drupal::messenger()->addError($error_message);
      \Drupal::logger('sms_netgsm')->error($method . ': ' . $e->getMessage());

      return $content;
    }
  }

  /**
   * Get error conditions for sending SMS HTTP request.
   *
   * @return array
   *   Returns an associative array containing SMS error conditions.
   */
  protected function getDefaultStatusConditions() : array {
    return [
      '20' => 'Invalid characters or exceeded the standard maximum number of message characters.',
      '30' => 'Invalid username or password.',
      '40' => 'Invalid sender name.',
      '50' => 'IYS controlled submissions cannot be made with your subscriber account.',
      '51' => 'Indicates that the IYS Brand information defined for your subscription is not found.',
      '70' => 'Invalid query parameters.',
      '80' => 'Sending limit exceeded.',
      '85' => 'Duplicate submission limit exceeded. More than 20 tasks cannot be created for the same number within 1 minute.',
      '100' => 'Netgsm system failure.',
      '101' => 'Netgsm system failure.',
    ];
  }

}

<?php

namespace Drupal\sms_netgsm\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\sms\Plugin\SmsGatewayPluginInterface;
use Drupal\sms_netgsm\NetgsmSmsReport;
use GuzzleHttp\Client;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides controller for Netgsm SMS report.
 */
class SmsReportController extends ControllerBase {

  /**
   * Get the Netgsm report for a specific bulk ID using the provided plugin.
   *
   * @param \Drupal\sms\Plugin\SmsGatewayPluginInterface|null $plugin
   *   (optional) The SMS gateway plugin instance. Defaults to NULL.
   * @param string|null $bulk_id
   *   (optional) The bulk ID for which the report is requested.
   *   Defaults to NULL.
   *
   * @example /sms/netgsm/netgsm_send_to_phone/report/1628674995
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   The JSON response containing the SMS report data.
   */
  public function view(SmsGatewayPluginInterface $plugin = NULL, string $bulk_id = NULL) {
    // Get the plugin configuration.
    $configuration = $plugin->getConfiguration();

    // Create HTTP client.
    $client = new Client();

    // Get the SMS report.
    $report = new NetgsmSmsReport();
    $response = $report
      ->setClient($client)
      ->setCredentials([
        'usercode' => $configuration['username'],
        'password' => $configuration['password'],
      ])
      ->setBulkId($bulk_id)
      ->setDetail(1)
      ->setType(0)
      ->setVersion(2)
      ->getResponse();

    // Create a JSON response.
    return new JsonResponse($response);
  }

}

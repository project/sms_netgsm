<?php

namespace Drupal\sms_netgsm;

use Drupal\Core\Url;

/**
 * Represents a SMS for Netgsm SMS.
 */
final class NetgsmSms {

  use NetgsmClient;

  /**
   * The SMS message.
   *
   * @var string
   */
  protected $message;

  /**
   * The SMS recipient.
   *
   * @var string
   */
  protected $recipient;

  /**
   * The SMS sender.
   *
   * @var string
   */
  protected $sender;

  /**
   * Set the SMS message.
   *
   * @param int $message
   *   The SMS message.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setMessage(string $message) {
    $this->message = $message;
    return $this;
  }

  /**
   * Set the SMS recipient.
   *
   * @param int $recipient
   *   The SMS recipient.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setRecipient(string $recipient) {
    $this->recipient = $recipient;
    return $this;
  }

  /**
   * Set the SMS sender.
   *
   * @param int $sender
   *   The SMS sender.
   *
   * @return $this
   *   Returns the called object for chaining.
   */
  public function setSender(string $sender) {
    $this->sender = $sender;
    return $this;
  }

  /**
   * Send and get the response from the URL and parse it.
   *
   * @see https://www.netgsm.com.tr/dokuman/#http-post-rapor
   *
   * @return array
   *   Returns an array containing parsed response information.
   */
  public function send() {
    // Add status conditions.
    $conditions = $this->getStatusConditions();
    $this->setConditions($conditions);

    $response = $this->call('POST', Url::fromUri('https://api.netgsm.com.tr/sms/send/get'), [
      'gsmno' => $this->recipient,
      'message' => $this->message,
      'msgheader' => $this->sender,
    ]) ?? '';

    return $this->parseResponse($response);
  }

  /**
   * Get response conditions for sending SMS HTTP POST.
   *
   * @return array
   *   Returns an associative array containing SMS response conditions.
   */
  protected function getStatusConditions() {
    return [
      '00' => 'There is no error in the date format of your task.',
      '00 %bulk_id' => 'The SMS has been sent to Netgsm and is now in queue to be sent within 5 minutes.',
      '01' => 'There is an error in the message sending start date. It was changed to the system date and processed.',
      '02' => 'There is an error in the message sending termination date. It has been changed to the system date and processed. If the end date is entered less than the start date, the system adds 24 hours to the current date to the end date.',
      '347022009' => 'It shows that the SMS you sent has successfully reached our system. You can query the status of your message with this task.',
    ];
  }

  /**
   * Parse the response string into an array of tasks based on a delta value.
   *
   * @param string $response
   *   The response string to be parsed.
   *
   * @return array
   *   Returns an array containing parsed task information
   *   based on the delta value.
   */
  protected function parseResponse(string $response) : array {
    $conditions = $this->conditions;
    $tasks = explode(' ', $response);
    $status_code = $tasks[0] ?? NULL;
    return [
      'arguments' => [
        'phone_number' => $this->recipient,
        'message' => $this->message,
        'sender' => $this->sender,
      ],
      'bulk_id' => $tasks[1] ?? NULL,
      'status' => [
        'code' => $status_code,
        'message' => $conditions[$status_code] ?? NULL,
      ],
    ];
  }

}
